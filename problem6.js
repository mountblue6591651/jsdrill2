// Function to filter BMW and Audi cars from the inventory
function getSpecificCars(inventory) {
  // Check if the inventory is empty
  if (inventory.length === 0) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Filter cars based on the car_make property (BMW or Audi)
    let BMWAndAudi = inventory.filter((carInfo) => {
      if (carInfo.car_make === "BMW" || carInfo.car_make === "Audi") {
        return true;
      }
    });

    // Check if there are BMW or Audi cars
    if (BMWAndAudi.length > 0) {
      return BMWAndAudi;
    } else {
      // No BMW or Audi cars found
      console.log("No Cars Found");
      return null;
    }
  }
}

// Export the function to make it available for external use
module.exports = getSpecificCars;
