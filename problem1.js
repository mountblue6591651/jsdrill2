function getCarInfoById(inventory, id) {
  // Check if the inventory is empty
  if (inventory.length === 0) {
    console.log("Inventory is empty");
    return null;
  } else if (id > inventory.length || id <= 0) {
    // Check if the provided ID is out of bounds or invalid
    console.log("Invalid ID entered");
    return null;
  } else {
    // Use the filter method to find cars with the specified ID
    let carInfo = inventory.filter((car) => {
      return car.id === id;
    });

    // Check if carInfo is not an empty array (car found)
    if (carInfo.length > 0) {
      // Return the first (and only) element of the filtered array
      return carInfo[0];
    }
  }

  // If the function reaches this point, the car with the specified ID is not found
  console.log("Car not found");
  return null;
}

// Export the function to make it available for external use
module.exports = getCarInfoById;
