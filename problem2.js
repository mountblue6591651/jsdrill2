function getLastCarInfo(inventory) {
    // Check if the inventory is empty
    if (inventory.length === 0) {
      console.log("Inventory is empty");
      return null;
    } else {
      // Get the last car in the inventory
      let lastCar = inventory[inventory.length - 1];
      
      // Return the information of the last car
      return lastCar;
    }
  }
  
  // Export the function to make it available for external use
  module.exports = getLastCarInfo;
  