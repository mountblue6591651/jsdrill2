// Importing the inventory data and the sortCarsAlphabetically function
const inventory = require("../data.js");
const sortCarsAlphabetically = require("../problem3.js");

// Calling the sortCarsAlphabetically function with the inventory
let sortedCars = sortCarsAlphabetically(inventory);

// Checking if sortedCars is not null (i.e., inventory is not empty)
if (sortedCars) {
  // Printing the sorted cars to the console
  console.log(sortedCars);
}
