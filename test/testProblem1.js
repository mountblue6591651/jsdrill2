// Importing the inventory data and the getCarInfoById function
const inventory = require("../data.js");
const getCarInfoById = require("../problem1.js");

// Setting a sample ID for testing
let id = 33;

// Calling the getCarInfoById function with the inventory and ID
let carInfo = getCarInfoById(inventory, id);

// Checking if carInfo is not null (i.e., a valid car information is found)
if (carInfo) {
  // Printing the car information to the console
  console.log(
    `Car ${carInfo.id} is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`
  );
}
