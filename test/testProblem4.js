// Importing the inventory data and the getCarYears function
const inventory = require("../data.js");
const getCarYears = require("../problem4.js");

// Calling the getCarYears function with the inventory
let carYears = getCarYears(inventory);

// Checking if carYears is not null (i.e., inventory is not empty)
if (carYears) {
  // Printing the array of car years to the console
  console.log(carYears);
}
