// Importing the inventory data and the getOldCars function from the specified file
const inventory = require("../data.js");
const getOldCars = require("../problem5.js");

// Specifying the reference year for filtering old cars
let year = 2000;

// Calling the getOldCars function with the inventory and reference year
let oldCars = getOldCars(inventory, year);

// Checking if oldCars is not null (i.e., inventory is not empty)
if (oldCars) {
  // Printing the number of old cars to the console
  console.log(oldCars.length);
}
