// Importing the inventory data and the getLastCarInfo function
const inventory = require("../data.js");
const getLastCarInfo = require("../problem2.js");

// Calling the getLastCarInfo function with the inventory
let lastCarInfo = getLastCarInfo(inventory);

// Checking if lastCarInfo is not null (i.e., inventory is not empty)
if (lastCarInfo) {
  // Printing information about the last car to the console
  console.log(`Last car is a ${lastCarInfo.car_make} ${lastCarInfo.car_model}`);
}
