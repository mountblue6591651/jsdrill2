// Importing the inventory data and the getSpecificCars function from the specified file
const inventory = require("../data.js");
const getSpecificCars = require("../problem6.js");

// Calling the getSpecificCars function with the inventory
let BMWAndAudi = getSpecificCars(inventory);

// Checking if BMWAndAudi is not null (i.e., inventory is not empty)
if (BMWAndAudi) {
  // Printing the JSON representation of BMWAndAudi to the console with indentation for better readability
  console.log(JSON.stringify(BMWAndAudi, null, 2));
}
