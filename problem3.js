//Function to sort all cars alphabetically according to their model
function sortCarsAlphabetically(inventory) {
  // Check if the inventory is empty
  if (inventory.length === 0) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Use the sort() method with a callback function to compare car_model property
    let sortedCars = inventory.sort((a, b) => {
      if (a.car_model.toUpperCase() > b.car_model.toUpperCase()) {
        return 1; // a should be sorted after b
      } else if (a.car_model.toUpperCase() < b.car_model.toUpperCase()) {
        return -1; // a should be sorted before b
      } else {
        return 0; // a and b are equal in terms of sorting
      }
    });
    return sortedCars;
  }
}

// Export the function to make it available for external use
module.exports = sortCarsAlphabetically;
