// Importing the getCarYears function from the same directory
const getCarYears = require('./problem4');

// Import the function to get all car years from problem4.js
function getOldCars(inventory, year) {
  // Check if the inventory is empty
  if (inventory.length === 0) {
    console.log("Inventory is empty");
    return null;
  } else if (year < 1886) {
    // Check if the specified year is valid
    console.log("Invalid year, Cars were made after 1886");
    return null;
  } else {
    // Get an array of all car years from the inventory
    let carYears = getCarYears(inventory);

    // Filter old cars based on the specified year
    let oldCars = carYears.filter((carYear) => {
      return carYear > year;
    });

    // Check if there are old cars
    if (oldCars.length > 0) {
      return oldCars;
    } else {
      // No old cars found
      console.log(`No car in inventory was made after ${year}`);
      return null;
    }
  }
}

// Export the function to make it available for external use
module.exports = getOldCars;
