// Function to retrieve an array containing all car years from the inventory
function getCarYears(inventory) {
    // Check if the inventory is empty
    if (inventory.length === 0) {
      console.log("Inventory is empty");
      return null;
    } else {
      // Extract car years from each carInfo object in the inventory
      let carYears = inventory.map((carInfo) => {
        return carInfo.car_year;
      });
  
      // Return the array of car years
      return carYears;
    }
  }
  
  // Export the function to make it available for external use
  module.exports = getCarYears;
  